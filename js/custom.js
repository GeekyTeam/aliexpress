//Home Page Top
$('.homeCarousel').owlCarousel({
  dots : true,
  nav:true,
  slideSpeed : 300,
  loop: true,
  auto: true,
  slideSpeed : 300,
  paginationSpeed : 400,
  paginationSpeed : 400,
  autoplay:true,
  autoplayTimeout:3000,
  autoplayHoverPause:true,
  items : 1
});
//Home Page 2nd scr
$('#owl-demo2, #owl-demo3').owlCarousel({
  margin:10,
  loop:true,
	dots: true,
  autoWidth:true,
	nav:true,
  items:7
})
$('.prodCarousel').owlCarousel({
  loop:true,
  dots: false,
  margin:20,
  // autoplay:true,
  // autoplayTimeout:5000,
  // responsiveClass:true,
  responsive:{
    0:{
      items:1,
      nav:true
    },
    600:{
      items:3,
      nav:false
    },
    1000:{
      items:3,
      nav:true,
      loop:false
    }
  }
});
$('.prodViewCarousel').owlCarousel({
  loop:true,
  dots: false,
  margin:20,
  // autoplay:true,
  // autoplayTimeout:5000,
  // responsiveClass:true,
  responsive:{
    0:{
      items:1,
      nav:true
    },
    600:{
      items:3,
      nav:false
    },
    1000:{
      items:5,
      nav:true,
      loop:false
    }
  }
});
// Home Page Product carousel
// $('.prodImgCarousel').owlCarousel({
//   dots : true,
//   slideSpeed : 300,
//   loop: true,
//   auto: true,
//   slideSpeed : 300,
//   paginationSpeed : 400,
//   paginationSpeed : 400,
//   autoplay:true,
//   autoplayTimeout:2000,
//   autoplayHoverPause:true,
//   items : 1
// });
$('.prodImgCarousel').carousel({
  interval: 2000
})
//top left Menu
$('.menuHeaderContainer li').hover(function () {
    $(this).find('.overlay').stop().fadeIn();
}, function () {
    $(this).find('.overlay').stop().fadeOut();
});
//2nd level
$('.menuHeaderContainer li li').hover(function () {
    $(this).find('.overlaySecond').stop().fadeIn();
}, function () {
    $(this).find('.overlaySecond').stop().fadeOut();
});
$(document).ready(function(){
  var x = $('.divHeightOne').height();
  var y = $('.divWidthOne').width();
  $('.divHeightTwo').css({'height': x});
  $('.divWidthTwo').css({'width': y});
  $('#backToTop').click(function() {
    $('html, body').animate({ scrollTop: 0 }, 'slow');
    return false;
  });
  $('#bzoom').zoom({
    zoom_area_width: 300,
    autoplay_interval :3000,
    small_thumbs : 4,
    autoplay : false
  });
  $('.changeAddress').click(function(){
    $('.changeAddressWrapp').fadeIn();
    $('.checkoutAddress').hide();
  });
  $('.updateAddress').click(function(){
    $('.checkoutAddress').fadeIn();
    $('.changeAddressWrapp').hide();
  });
  $('.prodAddCart').click(function(){
    window.location = 'product-view.html';
  })
  $('.prodFav i').click(function(){
    window.location = 'wishlist.html';
  })
});
$(function(){
  /*$( '#accordion' ).accordion({
    heightStyle: 'content'
  });*/
  // toggle active class of accordion
  jQuery('.accordions .panel-heading').on('click', function () {
    jQuery('.accordions .panel-heading').removeClass('actives');
    $(this).addClass('actives');
  });
});
$(window).scroll(function() {
 if ($(this).scrollTop() > 200){
  $('.fixHeader').addClass('sticky');
   }
   else{
   // console.log(98899);
   $('.fixHeader').removeClass('sticky');
   }
});
